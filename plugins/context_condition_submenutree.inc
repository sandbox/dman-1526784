<?php

/**
 * @file Plugin for context.
 *   Exposes submenutree settings items as a context condition.
 *
 * This will let you trigger reactions (such as showing nav blocks)
 * depending on if the submenu trees are already doing the job or not.
 *
 * Based on context - context_condition_node_taxonomy and others.
 *
 * @author dman dan@coders.co.nz http://coders.co.nz/
 * @version 2012-04
 */
class context_condition_submenutree extends context_condition_node {
  /**
   * Override of condition_values().
   *
   * Publishes the settings we expect to save.
   */
  function condition_values() {
    if (module_exists('submenutree')) {
      // List the submenutree status that a node may have.
      $submenutree_options = array(
        'submenutree_enable' => t('Submenutree is enabled'),
        'NOT_submenutree_enable' => t('Submenutree is disabled'),
        'siblingmenutree_enable' => t('Siblingmenutree is enabled'),
        'NOT_siblingmenutree_enable' => t('Siblingmenutree is disabled'),
      );
    }
    else {
      $submenutree_options = array();
    }
    return $submenutree_options;
  }

  /**
   * Override of condition_form().
   *
   * Modifies the settings form in the context UI.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    $form['#type'] = 'checkboxes';
    return $form;
  }

  /**
   * Override of execute().
   *
   * Cehcks to see if the condition we manage is actually met.
   *
   * This is called directly from
   * submenutree_context_context_node_condition_alter()
   */
  function execute($node, $op) {
    if ($this->condition_used()) {
      // We have (at least) two options on the node to check.
      // Either of them being on or off are possible triggers.
      // (I could not find a 'negate' flag in the current UI)
      $options = array('submenutree_enable', 'siblingmenutree_enable');

      // I guess we could also check the submenutree_display values,
      // though the values for that are more complex, so no.

      // It seems odd we have to call $this->get_contexts() so often,
      // but that's how the others I found did it.

      // As we are re-using the context_node methods, we should also respect
      // its 'Set on node form' flag. This makes the code twice as big here.

      foreach ($options as $option) {

        // Is the option on? (not empty)
        foreach ($this->get_contexts($option) as $context) {

          // Check the node form option.
          // Early exit if it's not on.
          if ($op === 'form') {
            $options = $this->fetch_from_context($context, 'options');
            if (empty($options['node_form'])) {
              continue;
            }
          }
          // Otherwise continue with the actual check.

          if (!empty($node->$option)) {
            // The option is enabled.
            $this->condition_met($context, $option);
          }
        }

        // The flag being OFF is a trigger also.
        $not_option = 'NOT_' . $option;
        foreach ($this->get_contexts($not_option) as $context) {

          // Check the node form option.
          // Early exit if it's not on.
          if ($op === 'form') {
            $options = $this->fetch_from_context($context, 'options');
            if (empty($options['node_form'])) {
              continue;
            }
          }
          // Otherwise continue with the actual check.

          if (empty($node->$option)) {
            // The option is disabled
            $this->condition_met($context, $not_option);
          }
        }

      } // Each option flag.
    } // Condition is active.
  }
}
