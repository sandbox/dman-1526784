= Submenutree context =

== Extends submenutree to provide a context condition ==

This will let you trigger reactions (such as showing nav blocks)
depending on if the submenu trees are already doing the job or not.

When making a new context, options such as 'Submenutree is enabled' or
'Submenutree is disabled' will be available as new conditions.


This is a glue module to connect
http://drupal.org/project/context
http://drupal.org/project/submenutree

== Use case ==

We had built a site that was about half made up of content pages (leaf nodes)
and half made up of index or topic pages (branch nodes) that grouped the 
content pages together. 
These topic pages were pretty light in content, and only served to list the
child pages - using submenutree.

Client requirement was to show large disclaimers and copyright statements on all
content pages, but this was clunky and redundant on the topic pages.
I wanted to avoid splitting pages into two 'content types' when the distinction
between the two wasn't really as clear-cut as it sounds.
Other options (menu paths, or taxonomy tagging) were not reliable enough for 
the specific site architecture.
I considered adding a flag to 'show disclaimer or not' on a per-page basis,
but decided against adding complexity, as it's not true data.

Instead, I needed a condition that would allow me to place the disclaimer block
on pages that were NOT SHOWING A LIST OF CHILDREN - as that was a natural rule.

THIS MODULE lets us position the block on pages based on this new rule.

The code involved was bigger than I'd hoped. Most of the trouble was admin 
overhead dealing with ctools and context API guff. 
The actual trigger part was simple.

== TODO ==

It's possible this could be extended to do the opposite - to <em>enable</em>
submenutree behavior based on context-triggered conditions.
Though I don't have a use for that right now.


.dan.